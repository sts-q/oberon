/* ==========================================================================*/

/* add.c  --  Additions to runtime functions of vishap voc. *)

/* included by Sys.Mod  */

/* ==========================================================================*/
#include <assert.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>
/*---------------------------------------------------------------------------*/
/* Add two integer values */
int add(int i, int j) {
return (int)i + j;
}

/* Exchange two integer values */
int xchg(int *i, int *j) {
int temp;
temp = *i;
*i = *j;
*j = temp;
}

/* int cbitAnd(int i, int j) { */
/* return i & j; */
/* } */

int cbitOr(int i, int j) {
return i | j;
}

int cbitXor(int i, int j) {
return i ^ j;
}


/* print chars to stdin */
void cOutChars(char *buffer, long size, long offset, long len) {
if (len > 0) {
	assert( offset + len <= size );
	int res = write(1, buffer + offset, len);
	assert(res == len);
}
}


static struct timespec req,rem;

void cnanosleep (long s, long ns) {
	req.tv_sec  = s;
	req.tv_nsec = ns;
	nanosleep (&req, &rem);
}


/*---------------------------------------------------------------------------*/
/* Get terminal size. */
short databuffer[2];

void xtermSize(int *lines, int *cols) {
        *lines = 0;
        *cols  = 0;
        ioctl(0, TIOCGWINSZ, databuffer);
        *lines =databuffer[0];
        *cols  =databuffer[1];
}


/*---------------------------------------------------------------------------*/
void SwitchToNonBlockingStdinIO() {
        fcntl(0, F_SETFL, O_NONBLOCK);
}


/*---------------------------------------------------------------------------*/
/* Readline:  Read line form stdin.
 * Return line without trailing <lf>.
 * Return -1 on failure.
 * Ctrl-D is returned as -1, too.
 */
ssize_t getline(char **lineptr, size_t *n, FILE *stream);

int cReadLine(char *line, int lineSize) {
        int len = getline(&line, (size_t *)(&lineSize), stdin);
        if (len > 0) {
                assert(len < lineSize);
                len = len - 1;
                line[len] = 0;
                return len;
        } else {
                line[0] = 0;
                return -1;
        }
}

/* Read chars from stdin into kbdBuffer.
 * Return number of read chars.
 * Return 0 if no chars available.
 * Return -errno on error.
 */
int cReadKeys(char *kbdbuffer, int KbdBufferSize) {
        int res;
        res = read(0, kbdbuffer, KbdBufferSize);
        if (res < 0) {
            if (errno = EAGAIN) {
                res = 0;
            } else {
                res = -errno;
            }
        }
        return res;
}


/*---------------------------------------------------------------------------*/
/* :section:    files */
int cFileExists( const char *name ) {
        if (access(name, F_OK) == 0)
                return true;
        else
                return false;
}

int cFileRead( const char *name, char *text, int textSize, int *text_len ) {
        FILE *fp;
        fp = fopen( name, "r" );
        if (!fp) return false;
        *text_len = (int)fread( text, 1, textSize, fp );
        /* buffer full -> file not fully read -> fail */
        assert( *text_len < textSize );
        fclose(fp);
        text[*text_len] = 0;
        return true;
}

int cFileWrite( const char *name, const char *text, int textLen ) {
        FILE *fp;
        fp = fopen( name, "w" );
        if (!fp) return false;
        int len = fwrite( text, 1, textLen, fp );
        assert( len == textLen );
        fclose(fp);
        return true;
}

int cFileAppendLine (const char *name, const char *text, int textLen) {
        FILE *fp;
        char *nl = "\n";
        fp = fopen (name, "a");
        if (!fp) return false;
        int len = fwrite (text, 1, textLen, fp);
        assert (len == textLen);
        len = fwrite (nl, 1,1, fp);
        assert (len == 1);
        fclose (fp);
        return true;
}


struct stat statbuffer;
int stat(const char *pathname, struct stat *statbuf);

int cFileTouchTime( const char *name ) {
        int res = stat( name, &statbuffer );
        if (res == 0) {
                /* c last change */
                /* a last access */
                /* m last modification */
                int time = statbuffer.st_mtime;
                /* int time = statbuffer.st_atime; */
                /* int time = statbuffer.st_mtime; */
                return time;
        } else {
                return -1;
        }
}

/* ========================================================================= */
/* (c) sts-q 2022-Apr,Nov */
