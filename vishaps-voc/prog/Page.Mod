MODULE Page;   (* :file: Page.Mod  --  Pager using Viewer. *)
IMPORT Sys, Int, Str, Path, String, Line, Text, Viewer, o := Dot;

CONST
   Petfiles        = "q/oberon-voc/.petfiles";
   PetTextContents = "q/oberon-voc/.pettextcontents";
VAR
   petfilename, pettextcontents: ARRAY 128 OF CHAR;


PROCEDURE LoadFilePosition (scs: Viewer.Screen);
VAR
   petfiles: Text.T;
   pos,i: INTEGER;
   l: Line.T;
BEGIN
   l := Text.Path (scs.t);
   IF l^ = petfilename THEN
      scs.p.y0 :=  0;
      scs.p.cy :=  1;
      scs.p.cx := 80;
   ELSE
      IF Text.Load (petfiles, Petfiles, petfilename) THEN
         i := 0;
         WHILE (i < Text.Length (petfiles))
            & Text.HasLine  (l, petfiles, i)
            & ~Line.HasHead (l, Text.Path (scs.t))
         DO INC (i)
         END;
         IF (i < Text.Length (petfiles)) & (l # NIL) THEN
            pos := 0;
            IF Str.FindChar (l^, ' ', pos) THEN
               WHILE l[pos] = ' ' DO INC (pos); END; scs.p.y0 := SHORT (Int.Read (l^, pos));
               WHILE l[pos] = ' ' DO INC (pos); END; scs.p.cy := SHORT (Int.Read (l^, pos));
               WHILE l[pos] = ' ' DO INC (pos); END; scs.p.cx := SHORT (Int.Read (l^, pos));
            END
         END
      ELSE o.Error (".petfiles not found");
      END;
   END;
END LoadFilePosition;

PROCEDURE SaveFilePosition (scs: Viewer.Screen);
VAR
   a,filename: ARRAY 128 OF CHAR;
   petfiles: Text.T;
   l,lx: Line.T;
   pos: Int.T;
   i: INTEGER;
BEGIN
   a := "";
   pos := 0;
   l := Text.Path (scs.t);
   Path.FileName  (filename, l^);
   Str.PushStr (a, pos, l^);
   Str.PushTab (a, pos, 48);
   Str.PushChr (a, pos, ' ');
   Str.PushInt (a, pos, scs.p.y0, 8);
   Str.PushInt (a, pos, scs.p.cy, 8);
   Str.PushInt (a, pos, scs.p.cx, 8);
   Str.PushTab (a, pos, 79);
   Str.PushChr (a, pos, ' ');
   Str.PushStr (a, pos, filename);
   IF Text.Load (petfiles, Petfiles, petfilename) THEN
      FOR i := 0 TO Text.Length (petfiles) - 1 DO
         IF Text.HasLine (lx, petfiles, i) & Line.HasHead (lx, Text.Path (scs.t)) THEN
            Text.DeleteLine (petfiles, i)
         END
      END;
      IF (l^ = petfilename) OR (l^ = pettextcontents) THEN
         Text.AppendLine (petfiles, Line.Create (a));
      ELSE Text.InsertLine (petfiles, 0, Line.Create (a));
      END;
      IF ~Text.Save (petfiles) THEN o.Error (".petfile saveing failed") END
   ELSE o.Error (".petfile not found.")
   END;
END SaveFilePosition;

PROCEDURE Load (VAR t: Text.T; name, path: ARRAY OF CHAR);
BEGIN
   IF ~Text.Load (t, name, path) THEN
      o.Errmess (path);              Sys.OutChr (0DX);
      o.Error   ("File not found."); Sys.OutChr (0DX);
   END;
END Load;

PROCEDURE ChangeFile (VAR name, path: ARRAY OF CHAR; scs:Viewer.Screen);        (* _xmark_ *)
VAR
   l: Line.T;
   pos: INTEGER;
BEGIN
   IF path # petfilename THEN
      Str.Copy (name, ".petfiles");
      Str.Copy (path, petfilename);
   ELSE
      IF Text.HasLine (l, scs.t, scs.p.cy) THEN
         pos := 0;
         IF Str.FindChar (l^, ' ', pos) THEN
            Str.Head      (path, l^, pos);
            Path.FileName (name, path);
         ELSE o.Error ("Page.ChangeFile: no space char found")
         END
      ELSE o.Error ("Page.ChangeFile: not at a valid line")
      END;
   END;
END ChangeFile;

PROCEDURE DoTextContents (VAR name, path: ARRAY OF CHAR; VAR cy:INTEGER; scs:Viewer.Screen);
VAR
(*    textname: ARRAY 32 OF CHAR; *)
   ot: Text.T;
   n: Line.T;
   l: Line.T;
   i: INTEGER;
   pos: INTEGER;
BEGIN
   n := Text.Path(scs.t);
   IF n^ # pettextcontents THEN
      IF Text.Contents (ot, scs.t) THEN
(*          textname := "Contents"; *)
(*          Text.SetName (ot, textname); *)
         Text.SetPath (ot, pettextcontents);
         Text.InsertLine (ot, 0, n);
         IF Text.Save (ot) THEN
            Str.Copy (name, "textcontents");
            Str.Copy (path, pettextcontents);
         ELSE o.Error ("Page.DoTextContents: Text.Save failed.")
         END;
      ELSE o.Lf; o.Error ("Page.DoTextContent: got no contents")
      END
   ELSE
      n := Text.GetLine (scs.t, 0);
      Str.Copy (path, n^);
      Path.FileName (name, path);
      IF Text.HasLine (l, scs.t, scs.p.cy) THEN
         pos := 0;
         WHILE l[pos] = ' ' DO INC (pos) END;
         cy := SHORT (Int.Read (l^, pos));
      ELSE o.Error ("Page.DoTextContents: no such line")
      END
   END
END DoTextContents;

PROCEDURE Start (name, path: ARRAY OF CHAR);
VAR
   t: Text.T;
   scs: Viewer.Screen;
   pos: Viewer.Pos;
   xw,xh: INTEGER;
   md: INTEGER;
   l: Line.T;
   cy: INTEGER;
BEGIN
   cy := -1;
   REPEAT
      Sys.XtermSize (xh, xw);
      Load (t, name, path);
      NEW (pos);
      pos.y0 := 0;
      pos.cy := 1;
      pos.cx := 1;
      NEW (scs); scs := NIL;
      Viewer.Append (scs, Viewer.NewScreen ("main",  2,1, xh-1, xw,  t, pos));
      Viewer.exitOnIterfile := FALSE;
      LoadFilePosition (scs);
      Sys.XtermAlt;
      IF cy > 0 THEN
         scs.p.y0 := cy;
         scs.p.cy := cy;
      END;
      IF Text.Length (scs.t) = 0 THEN
         Text.AppendLine (scs.t, Line.Create ("<-- dummyline -->"))
      END;
      Viewer.Start (md, scs);
      Sys.XtermStd;
      IF Text.IsModified (t) THEN
         IF Text.Save (t) THEN
         ELSE
            l := Text.Path (t); o.Errmess (l^);
            l := Text.Name (t); o.Errmess (l^);
            o.Error   ("Page.Start: Text.Save failed.")
         END;
      ELSE
      END;
      SaveFilePosition (scs);
      cy := -1;
      IF    md = Viewer.MdChangeFile   THEN ChangeFile     (name, path, scs);
      ELSIF md = Viewer.MdTextContents THEN DoTextContents (name, path, cy, scs)
      END;
   UNTIL (md = Viewer.MdExit);
END Start;

PROCEDURE Page;
VAR 
   path,p: ARRAY 256 OF CHAR;
   name: ARRAY 64 OF CHAR;
BEGIN
   IF Sys.Argc() = 3 THEN
      IF ~Sys.Argv (Viewer.currentSearchStr, 2) THEN
         o.Error ("argc 2: not received.")
      END
   END;
   IF Sys.Argc() > 1 THEN
      IF Sys.Argv (p, 1) THEN
         IF (Str.Length (p) > 0) & (p[0] = '/') THEN
            Str.Copy (path, p);
         ELSE
            Str.Copy   (path, Sys.pwd);
            Str.Append (path, p);
         END;
         Path.FileName (name, path);
         Start (name, path)
      ELSE o.Error ("Page: strange error (this should not happend)")
      END
   ELSE 
      name := ".petfiles";
      path := petfilename;
      Start (name, path);
(*       o.Errmess ("Number of received args:"); *)
(*       o.Errint  (Sys.Argc()); *)
(*       o.Errmess ("One argument as filename expected."); *)
(*       o.Error   ("Second argument as initial search string optional."); *)
   END;
END Page;


(* ------------------------------------------------------------------------- *)
(* :chapter: MAIN *)
BEGIN

   Str.Copy   (petfilename, Sys.home);
   Str.Append (petfilename, Petfiles);
   Str.Copy   (pettextcontents, Sys.home);
   Str.Append (pettextcontents, PetTextContents);
   Page;

END Page.
(* ------------------------------------------------------------------------- *)
(* :file: END sts-q 2024-Feb,Jun *)
