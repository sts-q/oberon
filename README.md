# Oberon

Oberon programming. -- Writing computer programs without stackoverflow.


# Links

[ References vishaps/voc does link to. ]( https://github.com/vishaps/voc#references )

[ SKL ]( https://github.com/thutt/oberon )

[ vishaps-voc ]( https://vishap.oberon.am ) 

[ oberon-compiler ]( https://github.com/ericscharff/oberon-compiler )

[ The Programming Language Oberon-7 ( OBNC and oberon-compiler)]( http://miasap.se/obnc/oberon-report.html )

[ The Programming Language Oberon-2 ( SKL and Voc )]( https://ssw.jku.at/Research/Papers/Oberon2.pdf )

[ Fyodor Tkachovi 2013: Less is more. Why Oberon beats mainstream ...]( http://www.zinnamturm.eu/doc/ACAT_2013_2_oberon_PUB.pdf )

[ Robert Giesemer 2015: The Evolution of Go.]( https://talks.golang.org/2015/gophercon-goevolution.slide#1 )

[ Eduard Ulrich at Barcamp Yerevan 2017:  C++, Java, Oberon - Pros and Cons.]( https://www.youtube.com/watch?v=SN-WF_bfs1c )

[ Niklaus Wirth, Abschiedvorlesung 18. Januar 1999]( https://video.ethz.ch/speakers/lecture/58ab4179-7033-462b-b26a-1b4bbf486fcf.html )
 
[ Oberon Day 2011 at ETH ]( https://video.ethz.ch/events/2011/oberon.html )

[ Oberon syntax hightlighting for vis.]( https://codeberg.org/sts-q/vis-editor/src/branch/master/oberon.lua )

