MODULE ssqDots;

IMPORT  Console, Environment, CommandLine,
        S := Strings, T := ssqTypes, I := ssqInts, Ink := ssqInks;


CONST
  floatwidth = 16;


VAR
  width - :      INTEGER;
  height - :     INTEGER;

  atLineStart:   T.INT;
  currentInk:    ARRAY 32 OF CHAR;
  currentGrink:  ARRAY 32 OF CHAR;


(*---------------------------------------------------------------------------*)
PROCEDURE spc*;                                      (* print one space char *)
BEGIN
  Console.Ch( ' ' );
  INC( atLineStart );
END spc;

PROCEDURE tab*( pos : INTEGER );         (* print spc until cursor is at pos *)
BEGIN
  WHILE atLineStart < pos DO
    spc;
  END
END tab;

PROCEDURE lf*;                        (* print linefeed if not at_line_start *)
BEGIN
  IF atLineStart # 0 THEN
    Console.Ch( CHR( 10 ));
    atLineStart := 0
  END
END lf;

PROCEDURE lf1*;                 (* print linefeed and set at_line_start := 0 *)
BEGIN
  Console.Ch( CHR( 10 ));
  atLineStart := 0
END lf1;



PROCEDURE lff*;                                      (* lf plus 1 empty line *)
BEGIN
  lf;
  Console.Ch( CHR( 10 ));
END lff;


PROCEDURE Char*( c : CHAR );
BEGIN
  Console.Ch( c );
  INC( atLineStart );
END Char;


PROCEDURE Str*( str : S.String );
BEGIN
  Console.Str( str^ );
  INC( atLineStart, S.Length( str ));
END Str;

PROCEDURE Mess*( s: ARRAY OF CHAR );
BEGIN
  Console.Str( s );
  INC( atLineStart, S.AOCLength( s ));
END Mess;

PROCEDURE Int*( i: LONGINT );
BEGIN
  Str( I.Show( i ));
  (* spc; *)
END Int;

PROCEDURE Flt*( flt: T.FLT );
BEGIN
  Console.LReal( flt, 16 ); spc;
  INC( atLineStart, floatwidth );
END Flt;

PROCEDURE Bool*( b: BOOLEAN );
BEGIN
    IF b THEN
      Mess( "TRUE " )
    ELSE
      Mess( "FALSE " )
    END
END Bool;

(*---------------------------------------------------------------------------*)
PROCEDURE Csi( s: ARRAY OF CHAR );
(* control sequence initiator: escape sequences *)
VAR
  i: LONGINT;
  str: S.String;
BEGIN
  str := S.New( 8, 0X );
  str[0] := CHR( 27 );
  str[1] := "[";
  i := 2;
  S.PushAOC( str,i, s );
  ASSERT( i # -1, 9000 );
  Console.Str( str^ );
END Csi;


PROCEDURE In*( rgb : ARRAY OF CHAR );           (* print in foreground color *)
VAR
  i: LONGINT;
  str: S.String;
BEGIN
  IF rgb # currentInk THEN
    str := S.New( 20, 0X );
    i := 0;
    S.PushChar( str,i, CHR(27));
    S.PushAOC(  str,i, "[38;2;" );
    S.PushAOC(  str,i, rgb );
    S.PushChar( str,i, "m" );
    Console.Str( str^ );                         (* don't count for at-line-start *)
    ASSERT( i # -1, 9000 );
    COPY( currentInk, rgb );
  END
END In;

PROCEDURE On*( rgb : ARRAY OF CHAR );  (* grounding ink: on background color *)
VAR
  i: LONGINT;
  str: S.String;
BEGIN
  IF rgb # currentGrink THEN
    str := S.New( 20, 0X );
    i := 0;
    S.PushChar( str,i, CHR(27) );
    S.PushAOC(  str,i, "[48;2;" );
    S.PushAOC(  str,i, rgb );
    S.PushChar( str,i, "m" );
    ASSERT( i # -1, 9000 );
    Console.Str( str^ );                         (* don't count for at-line-start *)
    COPY( currentGrink, rgb );
  END
END On;

PROCEDURE Clear*;                                            (* clear screen *)
BEGIN
  Csi( "2J" );
  Csi( "1;1H" );
END Clear;

PROCEDURE ClearLineTail*;
BEGIN
  Csi( "0K" );
END ClearLineTail;

PROCEDURE ClearLineHead*;
BEGIN
  Csi( "1K" );
END ClearLineHead;

PROCEDURE ClearLine*;
BEGIN
  Csi( "2K" );
  Char( CHR( 13));
END ClearLine;

PROCEDURE At*( line, column: INTEGER );             (* move cursor: print At *)
(** left top is 1,1
  * first column:   1         | -width
  * last column:    width     | 0
  * prelast column: width - 1 | -1
  *)
VAR
  i: LONGINT;
  str: S.String;
BEGIN
  IF line <= 0 THEN
    line := height + line + 1
  END;
  IF column <= 0 THEN
    column := width + column + 1
  END;
  str := S.New( 16, 0X );
  i := 0;
  S.PushChar( str,i, CHR(27));
  S.PushChar( str,i, "[" );
  S.PushInt(  str,i, line );
  S.PushChar( str,i, ";" );
  S.PushInt(  str,i, column );
  S.PushChar( str,i, "H" );
  ASSERT( i # -1, 9000 );
  Console.Str(  str^ );
END At;


(*---------------------------------------------------------------------------*)
PROCEDURE AsSection*;
BEGIN
  In( Ink.BROWN );
END AsSection;

PROCEDURE AsText*;
BEGIN
  In( Ink.LIGHTGRAY );
END AsText;

PROCEDURE AsComment*;
BEGIN
  In( Ink.GRAY );
END AsComment;

PROCEDURE AsDbg*;
BEGIN
  In( Ink.MAGENTA );
END AsDbg;

PROCEDURE AsErr*;
BEGIN
  In( Ink.RED );
END AsErr;

PROCEDURE AsStd*;
BEGIN
  In( Ink.WHITE );
END AsStd;


(*---------------------------------------------------------------------------*)
PROCEDURE Section*( mess : ARRAY OF CHAR );
BEGIN
  lff; AsSection; Mess( "-------  " ); Mess( mess );
  lf; AsText;
END Section;

PROCEDURE Error*( mess, val :ARRAY OF CHAR );      (* print errmess and exit *)
BEGIN
  lff;
  AsErr;
  lf; Mess( "####### Error" );
  lf; Mess( mess );
  lf; Mess( val );
  AsStd;
  lff;
  (* ASSERT( FALSE ); *)
  HALT( 256 );
END Error;

PROCEDURE SWW*( mess :ARRAY OF CHAR );             (* this should not happen *)
BEGIN
  lff;
  AsErr;
  lf; Mess( "####### Something went wrong:" );
  lf; Mess( mess );
  AsStd;
  lff;
  ASSERT( FALSE );
  (* HALT( 0 ); *)
END SWW;


PROCEDURE ROADWORK*( mess :ARRAY OF CHAR ); (* some feature is not yet there *)
BEGIN
  lff;
  AsErr;
  lf; Mess( "####### Roadwork" );
  lf; Mess( mess );
  AsStd;
  lff;
  ASSERT( FALSE );
END ROADWORK;


(*---------------------------------------------------------------------------*)
PROCEDURE InitSize;
CONST
  defaultWidth  = 80;
  defaultHeight = 25;
VAR
  cols, lines: Environment.Text;
  pos, value: LONGINT;
BEGIN
  cols := Environment.Lookup( "COLUMNS" );
  lines := Environment.Lookup( "LINES" );

  pos := 0;
  IF (cols # NIL) & S.ReadInt( S.Create( cols^ ), pos, value )  THEN
    width := SHORT( value )
  ELSE
    width := defaultWidth
  END;

  pos := 0;
  IF (lines # NIL) & S.ReadInt( S.Create( lines^ ), pos, value )  THEN
    height := SHORT( value )
  ELSE
    height := defaultHeight
  END;
  lf; Mess( "Xterm size:  "); Int( width ); Char( "x" ); Int( height );
END InitSize;

PROCEDURE InitArgs;
VAR
  pos: INTEGER;
  arg: CommandLine.Parameter;
BEGIN
  pos := 0;
  arg := CommandLine.GetArg( pos );
  WHILE arg # NIL DO
    lf; Mess( "commandline[" );Int( pos ); Mess( "]:   <" ); Mess( arg^ );  Char( ">" );
    INC( pos );
    arg := CommandLine.GetArg( pos );
  END;
  lf;
END InitArgs;


(*---------------------------------------------------------------------------*)
BEGIN
  InitSize;
  InitArgs;
  currentInk := "";
  currentGrink := "";
  atLineStart := 0;
END ssqDots.   (* (c) sts-q 2021-Aug *)
