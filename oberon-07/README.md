
## Oberon-07 Modules

Some Modules for Oberon-07.


## License

        ;=====================================================================
        ;   Oberon-07 Modules
        ;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
        ;   https://codeberg.org/sts-q/oberon
        ;   Copyright (C) 2022 2023 2024 sts-q
        ;   https://sts-q.codeberg.page
        ;=====================================================================
        ;   This program is distributed in the hope that it will be useful,
        ;   but WITHOUT ANY WARRANTY; without even the implied warranty of
        ;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        ;   GNU General Public License for more details.
        ;=====================================================================


## System Requirements

        Linux

        Eric Scharff's Oberon-compiler
           In order to compile these files global variable
           'ignoreKeywordCase' must be set to FALSE in compiler/Lex.ob.

        gcc


## Oberon-Compiler -- The Invisible Dragon.

Oberon-compiler is fun to work with. It just compiles. That's it!

No questions, no configs, not even a man page necessary.
At some point you forget it's even there.

This is *my* experience, that i made while i put together what is in this repo.
Other people have other constraints.


[oberon-compiler]( https://github.com/ericscharff/oberon-compiler )

An Oberon-07 compiler written in Oberon, with a bootstrap Oberon to C transpiler.
(c) Eric Scharff.


## Contact

[ sts-q ]( https://sts-q.codeberg.page/ )

First published in September 2022, last update September 2024.

