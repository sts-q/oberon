/* ==========================================================================*/
/* :file: add.c  --  Additions to runtime functions of oberon-compiler.      */
/* included by `cat compiler/runtime.c add.c` >build/runtime.c               */

/* int Xterm */
/* ==========================================================================*/
#define _GNU_SOURCE
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>

/* #include <sys/stat.h> */
/* #include <stdlib.h> */
/* #include <stdio.h> */

typedef __int64_t  INTEGER;
typedef __uint64_t U_INTEGER;


/*---------------------------------------------------------------------------*/
void say( const char *s ) {
        fputc( 13, stdout );
        fputc( 10, stdout );
        fputs( "#######  ", stdout);
        fputs( s, stdout);
}
void sayFlush () {
        say("said ]]");
        fputc( 13, stdout );
        fputc( 10, stdout );
}


/*---------------------------------------------------------------------------*/
/* print chars to stdin */
void Lib_OutChars(const char *buffer, int size, int offset, int len) {
        if (len > 0) {
                assert( offset + len <= size );
                int res = write(1, buffer + offset, len);
                assert(res == len);
        }
}

/* Halt */
void Lib_Halt(int i) {
        exit(i);
}
/*---------------------------------------------------------------------------*/
/* Add two integer values */
int Sys_Add(int i, int j) {
        return i + j;
}

/* Exchange two integer values */
void Sys_Xchg(int *i, int *j) {
        int temp = *i;
        *i = *j;
        *j = temp;
}

INTEGER Sys_Muldivmod( INTEGER *mod,   INTEGER i, INTEGER j, INTEGER k ) {
        /* should use some int128_t for the intermediate result, now */
        INTEGER res;
        __int128_t l = (__int128_t)i * (__int128_t)j;
        res  = l / k;
        *mod = l % k;
        return res;
}

INTEGER Sys_Bitshl (INTEGER i, INTEGER j) {
        return i << j;
}

INTEGER Sys_Bitshr (INTEGER i, INTEGER j) {
        INTEGER res = (U_INTEGER)i >> j;
        return res;
}

INTEGER Sys_Bitasr (INTEGER i, INTEGER j) {
        return i >> j;
}


/*---------------------------------------------------------------------------*/
/* :section:    stdout */
/* Get terminal size. */
short databuffer[2];
void Sys_XtermSize(INTEGER *lines, INTEGER *cols) {
        ioctl(0, TIOCGWINSZ, databuffer);
        *lines = databuffer[0];
        *cols  = databuffer[1];
}

void Sys_GoNonblocking() {
        fcntl(0, F_SETFL, O_NONBLOCK);
}


/*---------------------------------------------------------------------------*/
/* :section:    stdin */

/* Readline:  Read line form stdin.
 * Return line without trailing <lf>.
 * Retturn -1 on failure.
 * Ctrl-D is returned as -1, too.
 */
ssize_t getline(char **lineptr, size_t *n, FILE *stream);

int Sys_ReadLine(char *line, int line_size) {
        int len = getline(&line, (size_t *)(&line_size), stdin);
        if (len > 0) {
                assert(len < line_size);
                len = len - 1;
                line[len] = 0;
                return len;
        } else {
                line[0] = 0;
                return -1;
        }
}

/* Read chars from stdin into kbdBuffer.
 * Return number of read chars.
 * Return 0 if no chars available.
 * Return -errno on error.
 */
int Sys_ReadKeys(char *kbdbuffer, int KbdBufferSize) {
        int res;
        res = read(0, kbdbuffer, KbdBufferSize);
        if (res < 0) {
            if (errno = EAGAIN) {
                res = 0;
            } else {
                res = -errno;
            }
        }
        return res;
}

/*---------------------------------------------------------------------------*/
/* :section:    system*/
/* get environment vars */
int Sys_Syscall (INTEGER scall, INTEGER rdi, INTEGER rsi, INTEGER rdx) {
        int res = syscall (scall, rdi, rsi, rdx);
        return res;
}

int Sys_Getenv(const char *name, int name_size, char *value, int value_size) {
        char *v;
        int i;
        v = getenv(name);
        if (v == NULL) {
          value[0] = 0;
          return false;
        } else {
          for (i=0; (v[i] != 0) & (i < value_size); i++)
                value[i] = v[i];
          value[i] = 0;
          return true;
        }
}

/* sleep via nanosleep */
struct timespec sleep_req;
struct timespec sleep_rem;
int nanosleep( const struct timespec *req, struct timespec *rem);
int Sys_Nanosleep( int sec, int nsec ) {
        sleep_req.tv_sec  = sec;
        sleep_req.tv_nsec = nsec;
        int res = nanosleep( &sleep_req, &sleep_rem );
        return res;
}

/* get time in nanoseconds */
struct timespec nanotime;
// int CLOCK_REALTIME;
int clock_gettime(clockid_t clockid, struct timespec *tp);
int Sys_Nanotime( INTEGER *seconds, INTEGER *nanoseconds ) {
        int res = clock_gettime(CLOCK_REALTIME, &nanotime);
        *seconds     = nanotime.tv_sec;
        *nanoseconds = nanotime.tv_nsec;
        if (res == 0)
                return true;
        else
                return false;
}

/* get unix time */
int Sys_Time() {
        return time(0);
}

/* run shell command via system */
int Sys_cSystem (const char *command, int slen) {
        int status = system (command);
        INTEGER res = status;
        return res;
}


/*---------------------------------------------------------------------------*/
/* :section:    files */
int Sys_FileExists( const char *name, int name_len ) {
        if (access(name, F_OK) == 0)
                return true;
        else
                return false;
}

int Sys_FileRead( const char *name, int name_size, char *text, int text_size, INTEGER *text_len ) {
        FILE *fp;
        fp = fopen( name, "r" );
        if (!fp) return false;
        *text_len = (int)fread( text, 1, text_size, fp );
        assert( *text_len < text_size );   /* buffer full -> file not fully read -> fail */
        fclose(fp);
        text[*text_len] = 0;
        return true;
}

int Sys_FileWrite( const char *name, int name_size, const char *text, int text_size, int text_len ) {
        FILE *fp;
        fp = fopen( name, "w" );
        if (!fp) return false;
        int len = fwrite( text, 1, text_len, fp );
        assert( len == text_len );
        fclose(fp);
        return true;
}

                                          
struct stat statbuffer;
int stat(const char *pathname, struct stat *statbuf);

int Sys_FileTouchTime( const char *name, int name_size ) {
        int res = stat( name, &statbuffer );
        if (res == 0) {
                /* c last change */
                /* a last access */
                /* m last modification */
                int time = statbuffer.st_mtime;         
                /* int time = statbuffer.st_atime; */   
                /* int time = statbuffer.st_mtime; */   
                return time;
        } else {
                return -1;
        }
}

/* ========================================================================= */
/* (c) sts-q 2022-Apr */
