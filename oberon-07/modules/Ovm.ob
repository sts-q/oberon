MODULE Ovm;   (* :file: Ovm.ob  --  Stack Based Virtual Machine *)
IMPORT Lib, Sys, Int, o := OvmOp;


CONST
(*      CntMax = 5000000;                       (* maximum number of ovm instructions to execute *) *)
        CntMax = 0;                             (* 0 --> no limit *)
(*      OUT    = TRUE;                          (* print some debug info *) *)
        OUT    = FALSE;

        KiB           = 1024;
        MiB           = KiB * KiB;

        MemSize       =   8 * MiB;              (* memory for program, data and stack *)
        DspMax        = MemSize - 8;            (* small padding as saveguard until next overflow-check *)
(*         DspZero       = 131000;                 (* stack start address in mem, sort of 'hardwired' *) *)


TYPE
        T = INTEGER;                   (* 64-bit integer: bytecode instr: 24-bit-addr 32-bit-cnst 8-bit-code*)


VAR
        mem:            ARRAY MemSize OF T;     (* memory for all: program, global + local data, stack *)
        tos:            INTEGER;                (* Top Of Stack *)
        dsp:            INTEGER;                (* points full *)
        bsp:            INTEGER;                (* base pointer *)
        ip:             INTEGER;                (* instruction pointer *)
        here:           INTEGER;                (* fill pointer while receiving bytecode *)
        chunks:         INTEGER;                (* how many times Receive was called *)
        out:            BOOLEAN;                (* print debug info *)
        cnt:            INTEGER;                (* number of executed ovm instructions *)


(* ========================================================================= *)
(* :chapter: STANDARD PROCEDURES TO BE INLINED IN OTHER MODULES *)

PROCEDURE lf;                                   BEGIN Lib.Lf                    END lf;
PROCEDURE lff;                                  BEGIN Lib.Lff                   END lff;
PROCEDURE spc;                                  BEGIN Lib.Spc                   END spc;
PROCEDURE ddx;                                  BEGIN Lib.DDX                   END ddx;

PROCEDURE tab     (w: INTEGER);                 BEGIN Lib.Tab (w)               END tab;

PROCEDURE cprint  (c: CHAR);                    BEGIN Lib.Chr  (c)              END cprint;
PROCEDURE iprint  (n,w: INTEGER);               BEGIN Lib.Int  (n,w)            END iprint;
PROCEDURE bprint  (b: BOOLEAN);                 BEGIN Lib.Bool (b)              END bprint;

PROCEDURE print   (mess: ARRAY OF CHAR);        BEGIN Lib.Mess    (mess)        END print;
PROCEDURE section (mess: ARRAY OF CHAR);        BEGIN Lib.Section (mess)        END section;


(* ========================================================================= *)
(* :chapter: LIB *)
PROCEDURE opPrint (op, rop: INTEGER);           (* print ovm instruction: op lit addr *)
VAR a,b: INTEGER;
   name: ARRAY 32 OF CHAR;
BEGIN
   o.ShowCode (name, op);
   a := rop BITAND o.MASK32;
   b := rop / o.FACTOR32;
   ddx; print (name); spc; iprint (a, 0); iprint (b,0); ddx;
END opPrint;

PROCEDURE Out (text: ARRAY OF CHAR; value: INTEGER);
BEGIN
   print (" "); print (text); print ("="); iprint (value,0);
END Out;

PROCEDURE statusPrint;
BEGIN
   ddx; lf; print ("statusPrint:  [ip   bsp dsp rsp   tos cnt]  ");
            iprint (ip,  0);
            iprint (bsp, 8);
            iprint (dsp, 0);
            iprint (tos, 6);
            iprint (cnt, 0);
   ddx; lf;
END statusPrint;

PROCEDURE PushTos;
BEGIN INC (dsp); mem[dsp] := tos END PushTos;

PROCEDURE ReadInt (sb: ARRAY OF CHAR; pos: T): T;   (* read 64-bit integer from byte array *)
VAR i: INTEGER;
   res: T;
   c: CHAR;
BEGIN
   res := 0;
   FOR i := 0 TO 7 DO
      c := sb[pos + (7-i)];
      res := res * 256 + (256 + ORD (c)) MOD 256;     (* ??? *)
   END;
   RETURN res
END ReadInt;

PROCEDURE WriteInt (VAR text: ARRAY OF CHAR; n, pos: INTEGER);
VAR i: INTEGER;
BEGIN
   FOR i := 0 TO 7 DO
      text[pos + i] := CHR (n MOD 256);
      n := n DIV 256;
   END
END WriteInt;

PROCEDURE aPrint (addr: T);                     (* aPrint: do o.Print -- print string *)
VAR c: T;
BEGIN
   c := mem[addr];
   WHILE c # 0 DO
      cprint (CHR (c));
      INC (addr);
      c := mem[addr];
   END;
END aPrint;

PROCEDURE PrintStack;
CONST MAX = 8;
VAR k,i: INTEGER;
BEGIN
   Lib.AsErr; print ("ovm.Run:  ["); iprint (dsp, 0); print ("]   { ");
   k := Int.Max (0, dsp - MAX + 1);
   FOR i := k + 1 TO dsp DO iprint (mem[i], 0); END;
   print ("}");
   iprint (tos, 0);
   print ("  :: "); print ("stack not empty at ovm exit");
   Lib.AsStd; lf;
END PrintStack;


(* ========================================================================= *)
(* :chapter: DoRUN *)
PROCEDURE DoRun(): INTEGER;
VAR temp,instr,code,rop: INTEGER;
   syscall,rdi,rsi,rdx: INTEGER;
BEGIN
   cnt :=  0;
   WHILE TRUE DO
      instr := mem[ip];
      INC (ip);
      code := instr BITAND 255;
      rop := instr / 256;
      CASE code OF
          | o.Null:         Lib.Error ("ovm ran into NULL/WALL instr")
          | o.Pad:          ;
          | o.Unit:         DEC(dsp); mem[dsp] := tos; tos := 777
          | o.Zap:          tos := mem[dsp]; INC (dsp)
          | o.Dup:          DEC (dsp); mem[dsp] := tos
          | o.Swap:         temp := mem[dsp]; mem[dsp] := tos; tos := temp
          | o.Over:         DEC (dsp); mem[dsp] := tos; tos := mem[dsp+1]
          | o.Nip:          INC (dsp)
      (* :section: print *)
          | o.Lf:           lf
          | o.Lff:          lff
          | o.Spc:          spc
          | o.Print:        aPrint (tos);      tos := mem[dsp]; INC (dsp)
          | o.CPrint:       cprint (CHR(tos)); tos := mem[dsp]; INC (dsp)
          | o.IPrint:       iprint (tos, mem[dsp]); tos := mem[dsp-1]; INC (dsp, 2)

          | o.Syscall:
                             syscall := mem[dsp+2];
                             rdi     := mem[dsp+1];
                             rsi     := mem[dsp-0];
                             rdx     := tos;
                             INC (dsp, 3);
                             IF    syscall = 201 THEN tos := Sys.Syscall (syscall, rdi, rsi, rdx);
                             ELSIF syscall =  35 THEN tos := Sys.Nanosleep (rdi, rsi);
                             ELSE Lib.Error ("ovm: unknown syscall")
                             END;

      (* :section: exit jmp call ret *)
          | o.Exit:         RETURN dsp
          | o.Proc:         ;
          | o.Jmp:          ip := rop / o.FACTOR32
          | o.Jeq0:         IF tos =  0 THEN ip := rop / o.FACTOR32 END; tos := mem[dsp]; INC (dsp)
          | o.Jneq0:        IF tos #  0 THEN ip := rop / o.FACTOR32 END; tos := mem[dsp]; INC (dsp)

          | o.Call:
                            mem[dsp-1] := tos;
                            dsp := dsp - (rop BITAND o.MASK32) - 3;
                            mem[dsp+1] := bsp;
                            mem[dsp]   := ip;
                            ip  := rop / o.FACTOR32;
                            bsp := dsp;

          | o.CallInd:      (* currently only and exactly one argument and no locals possible *)
                            temp := mem[dsp];
                            DEC (dsp, 2);
                            mem[dsp+2] := tos;
                            mem[dsp+1] := bsp;
                            mem[dsp]   := ip;
                            ip  := temp;
                            bsp := dsp;

          | o.Ret:
                            (* dsp -> old tos *)
                            ip   := mem[dsp+1];
                            bsp  := mem[dsp+2];
                            dsp  := dsp + 3 + (rop BITAND o.MASK32);

      (* :section: load/store *)
          | o.PosLit:       DEC (dsp); mem[dsp] := tos; tos :=  rop
          | o.NegLit:       DEC (dsp); mem[dsp] := tos; tos := -rop
          | o.AsGlobal:     DEC (dsp); mem[dsp] := tos; tos :=  rop;
          | o.GlbLoad:      DEC (dsp); mem[dsp] := tos; tos := mem[rop]; Lib.Roadwork ("Ovm: look: o.GlbLoad ")
          | o.StkLoad:      DEC (dsp); mem[dsp] := tos; tos := mem[bsp + rop];
          | o.AsLocal:      DEC (dsp); mem[dsp] := tos; tos := bsp + rop
          | o.MemLoad:      tos := mem[tos]
          | o.MemStore:     mem[mem[dsp]] := tos; INC (dsp)
      (* :section: math ... *)
          | o.Not:          IF tos = 0 THEN tos := 1 ELSE tos := 0 END
          | o.Neg:          tos := -tos
          | o.Abs:          tos := ABS (tos)
          | o.Odd:          IF tos BITAND 1 = 1 THEN tos := 1 ELSE tos := 0 END
          | o.Inv:          tos := -1 - tos;
          | o.Sgn:          IF tos < 0 THEN tos := -1 ELSIF tos > 0 THEN tos := 1 ELSE tos := 0 END
          | o.Sqr:          tos := tos * tos
          | o.Sqrt:         tos := Int.Sqrt (tos)

          | o.CmpLt:        IF mem[dsp] <  tos THEN tos := 1 ELSE tos := 0 END; INC (dsp)
          | o.CmpLe:        IF mem[dsp] <= tos THEN tos := 1 ELSE tos := 0 END; INC (dsp)
          | o.CmpEq:        IF mem[dsp] =  tos THEN tos := 1 ELSE tos := 0 END; INC (dsp)
          | o.CmpNeq:       IF mem[dsp] #  tos THEN tos := 1 ELSE tos := 0 END; INC (dsp)
          | o.CmpGe:        IF mem[dsp] >= tos THEN tos := 1 ELSE tos := 0 END; INC (dsp)
          | o.CmpGt:        IF mem[dsp] >  tos THEN tos := 1 ELSE tos := 0 END; INC (dsp)
          | o.Add:          tos := mem[dsp] +   tos; INC (dsp)
          | o.Sub:          tos := mem[dsp] -   tos; INC (dsp)
          | o.Mul:          tos := mem[dsp] *   tos; INC (dsp)
          | o.Div:          IF tos = 0 THEN Lib.Error ("div by zero")    END; tos := mem[dsp] DIV tos; INC (dsp)
          | o.Mod:          IF tos = 0 THEN Lib.Error ("modulo by zero") END; tos := mem[dsp] MOD tos; INC (dsp)
          | o.And:          tos := mem[dsp] BITAND tos; INC (dsp)
          | o.Or:           tos := mem[dsp] BITOR  tos; INC (dsp)
          | o.Xor:          tos := mem[dsp] BITXOR tos; INC (dsp)
          | o.Shl:          tos := Sys.Bitshl (mem[dsp], tos); INC (dsp)
          | o.Shr:          tos := Sys.Bitshr (mem[dsp], tos); INC (dsp)
          | o.Asr:          tos := Sys.Bitasr (mem[dsp], tos); INC (dsp)
          | o.Max:          IF mem[dsp] >  tos THEN tos := mem[dsp] END; INC (dsp)
          | o.Min:          IF mem[dsp] <  tos THEN tos := mem[dsp] END; INC (dsp)
          | o.Power:        tos := Int.Power (mem[dsp], tos); INC (dsp)

          | o.CmpLtC:       IF tos <  rop THEN tos := 1 ELSE tos := 0 END
          | o.CmpLeC:       IF tos <= rop THEN tos := 1 ELSE tos := 0 END
          | o.CmpEqC:       IF tos =  rop THEN tos := 1 ELSE tos := 0 END
          | o.CmpNeqC:      IF tos #  rop THEN tos := 1 ELSE tos := 0 END
          | o.CmpGeC:       IF tos >= rop THEN tos := 1 ELSE tos := 0 END
          | o.CmpGtC:       IF tos >  rop THEN tos := 1 ELSE tos := 0 END
          | o.AddC:         tos := tos + rop
          | o.SubC:         tos := tos - rop
          | o.MulC:         tos := tos * rop
          | o.DivC:         IF rop = 0 THEN Lib.Error ("div by zero")    END; tos := tos DIV rop
          | o.ModC:         IF rop = 0 THEN Lib.Error ("modulo by zero") END; tos := tos MOD rop
          | o.AndC:         tos := tos BITAND rop
          | o.OrC:          tos := tos BITOR  rop
          | o.XorC:         tos := tos BITXOR rop
          | o.ShlC:         tos := Sys.Bitshl (tos, rop)
          | o.ShrC:         tos := Sys.Bitshr (tos, rop)
          | o.AsrC:         tos := Sys.Bitasr (tos, rop)
          | o.MaxC:         IF tos <  rop THEN tos := rop END
          | o.MinC:         IF tos >  rop THEN tos := rop END
          | o.PowerC:       tos := Int.Power (tos, rop)

          | o.CmpLtJ:       IF mem[dsp] <  tos THEN ip := rop/o.FACTOR32 END; tos := mem[dsp+1]; INC (dsp,2)
          | o.CmpLeJ:       IF mem[dsp] <= tos THEN ip := rop/o.FACTOR32 END; tos := mem[dsp+1]; INC (dsp,2)
          | o.CmpEqJ:       IF mem[dsp] =  tos THEN ip := rop/o.FACTOR32 END; tos := mem[dsp+1]; INC (dsp,2)
          | o.CmpNeqJ:      IF mem[dsp] #  tos THEN ip := rop/o.FACTOR32 END; tos := mem[dsp+1]; INC (dsp,2)
          | o.CmpGeJ:       IF mem[dsp] >= tos THEN ip := rop/o.FACTOR32 END; tos := mem[dsp+1]; INC (dsp,2)
          | o.CmpGtJ:       IF mem[dsp] >  tos THEN ip := rop/o.FACTOR32 END; tos := mem[dsp+1]; INC (dsp,2)

          | o.CmpLtCJ: IF tos <  (rop BITAND o.MASK32) THEN ip:=rop/o.FACTOR32 END; tos:=mem[dsp]; INC(dsp);
          | o.CmpLeCJ: IF tos <= (rop BITAND o.MASK32) THEN ip:=rop/o.FACTOR32 END; tos:=mem[dsp]; INC(dsp);
          | o.CmpEqCJ: IF tos =  (rop BITAND o.MASK32) THEN ip:=rop/o.FACTOR32 END; tos:=mem[dsp]; INC(dsp);
          | o.CmpNeqCJ:IF tos #  (rop BITAND o.MASK32) THEN ip:=rop/o.FACTOR32 END; tos:=mem[dsp]; INC(dsp);
          | o.CmpGeCJ: IF tos >= (rop BITAND o.MASK32) THEN ip:=rop/o.FACTOR32 END; tos:=mem[dsp]; INC(dsp);
          | o.CmpGtCJ: IF tos >  (rop BITAND o.MASK32) THEN ip:=rop/o.FACTOR32 END; tos:=mem[dsp]; INC(dsp);

         END; (* CASE *)

(*       No flags supported, anymore. *)
(*       IF ((instr BITAND 128) # 0) & o.FlagIND THEN *)
(*          tos := mem[tos]; *)
(*       END; *)
(*       IF CntMax # 0 THEN   (* with CntMax -> no CntMax: 8.3 --> 8.1 *) *)
(*          INC (cnt); *)
(*          IF cnt >= CntMax THEN Lib.Errint (CntMax);  Lib.Error ("CntMax of instr reached.") END; *)
(*       END; *)

      IF dsp < here    THEN Lib.Error ("ovm: dsp overflow")  END;   (* 2x Lib.Assert --> IF dsp: 16.2 --> 8.1 *)
      IF dsp >=DspMax  THEN Lib.Error ("ovm: dsp underflow") END;   (* better stay here at tail of loop *)
   END; (* WHILE TRUE *)
END DoRun;


(* ========================================================================= *)
(* :chapter: RUN *)
PROCEDURE Init*;                                (* to be called before usage *)
VAR i: INTEGER;
BEGIN
   FOR i := 0 TO MemSize - 1 DO mem[i] := 0 END;
   chunks  :=  0;
   here    :=  0;
   tos     :=  776;
   dsp     :=  DspMax;
   bsp     :=  dsp;
   ip      :=  0;
   out     :=  OUT;
   print ("ovm-init/ ");
END Init;

PROCEDURE Run* (VAR exitcode: INTEGER);         (* run loaded/received bytecode instructions *)
BEGIN
   IF DoRun () # (DspMax - 1) THEN              (* exitcode in tos *)
      statusPrint;
      PrintStack;
   END;
   IF out THEN statusPrint END;
   exitcode := tos;
END Run;


(* ========================================================================= *)
(* :chapter: LOAD *)
PROCEDURE Receive* (len: INTEGER; ts: ARRAY OF T);   (* store bytecode instrs im ovm memory *)
VAR i: INTEGER;
BEGIN
   Lib.Assert ((here + len) <= MemSize, "ovm.Receive: writes over upper mem limit");
   FOR i := 0 TO len - 1 DO
      mem[here] := ts[i];
      INC (here);
   END;
   INC (chunks);
END Receive;

PROCEDURE FixAt* (pos: INTEGER; t: T);          (* at pos fix 1 instr *)
BEGIN
   Lib.Assert ((0 <= pos) & (pos < MemSize), "ovm.FixAt: out of bounds");
   mem[pos] := t;
END FixAt;

PROCEDURE Save* (name: ARRAY OF CHAR);          (* save bytecode to file, e.g. for ovmf.fasm *)
VAR len,i: INTEGER;
   text: ARRAY 256000 OF CHAR;
BEGIN
   lf; print ("ovm.Save: "); print (name); iprint (here, 9); print ("words to write,");
   len := 0;
   FOR i := 0 TO here - 1 DO
      WriteInt (text, mem[i], len);
      INC (len, 8)
   END;
   IF Sys.FileWrite (name, text, len) THEN
      iprint (len,8); print ("bytes written");
      lf;
   ELSE
      Lib.Errmess (name);
      Lib.Error ("ovm.Save failed");
   END;
END Save;

PROCEDURE Load* (name: ARRAY OF CHAR): BOOLEAN;   (* roadwork: load bytecode *)
CONST SrcBufferSize = 128 * KiB;
VAR src: ARRAY 128 OF CHAR;
   srcBuffer: ARRAY SrcBufferSize OF CHAR;
   len,i,here: INTEGER;
   res: BOOLEAN;
BEGIN
   src := Sys.q;
   Lib.Append (src, "oberon-07/ovm/");
   Lib.Append (src, name);
   Lib.Append (src, ".ovm");
   IF Sys.FileRead (src, srcBuffer, len) THEN
      i    := 0;
      here := 0;
      WHILE i < len DO
         mem[here] := ReadInt (srcBuffer, i);
         INC (i, 8);
         INC (here);
      END;
      res := TRUE
   ELSE res := FALSE
   END;
   RETURN res;
END Load;

PROCEDURE Start* (fileName: ARRAY OF CHAR);     (* roadwork: init, load bytecode and run *)
VAR exitcode:INTEGER;
BEGIN
   Init;
   IF Load (fileName) THEN
      Run (exitcode)
   ELSE Lib.Errmess (fileName); Lib.Error ("Load failed.")
   END;
END Start;


(* ========================================================================= *)
(* :chapter: MAIN *)
BEGIN


(* ========================================================================= *)
END Ovm. :file: END sts-q 2024-Nov,Dec
