MODULE OvmOp;
IMPORT Lib;

CONST
        FlagIND*     = FALSE;                   (* no flags supported anymore *)

        Null*        = 0;                       (* WALL: exit with errormessage *)
        Pad*         = 1;                       (* NOP or padding: do nothing *)
        Unit*        = 2;                       (* push an empty value to stack like NIL, null or void *)
        Dup*         = 3;                       (*   {x -- x x}      *)
        Nip*         = 4;                       (*   {x y -- y]      *)
        Over*        = 5;                       (*   {x y -- x y x}  *)
        Swap*        = 6;                       (*   {x y -- y x}    *)
        Zap*         = 7;                       (*   {x -- }         *)

        Lf*          = 8;                       (* print a linefeed to stdout *)
        Lff*         = 9;                       (* print an empty line *)
        Spc*         = 10;                      (* print a space to stdout *)
        Print*       = 11;                      (* print a string *)
        CPrint*      = 12;                      (* print a character *)
        IPrint*      = 13;                      (* print an integer: {width int --}  *)

        Exit*        = 14;                      (* exit ovm: {exitcode --}  *)
        Proc*        = 15;                      (* enter procedure, does currently nothing, see 'call' *)
        Jmp*         = 16;                      (* jump:   [24-bit-absolute-address  32-bit-free  op]  *)
        Jeq0*        = 17;                      (* jcc: jump if (tos = 0)  {b --}  *)
        Jneq0*       = 18;                      (* jcc: jump if (tos # 0)  {b --}  *)
        Call*        = 19;                      (* call procedure, does proc enter sequence *)
        CallInd*     = 20;                      (* call indirect: call [tos] *)
        Ret*         = 21;                      (* return *)

        PosLit*      = 22;                      (* push integer:         { --  x}  [56-bit-int  8-bit-op]  *)
        NegLit*      = 23;                      (* push integer negated: { -- -x}  *)

        AsGlobal*    = 24;                      (* op constant/program-rel   *)
        GlbLoad*     = 25;                      (* op constant/program-rel   *)
        StkLoad*     = 26;                      (* op constant/bsp-rel       *)
        AsLocal*     = 27;                      (* op constant/bsp-rel       *)
        MemLoad*     = 28;                      (* op                        *)
        MemStore*    = 29;                      (* op                        *)

        Syscall*     = 30;                      (* syscall rdi dsi rdx       *)
        Free*        = 31;                      (* free... *)


        Not*         = 32;                      (*   logical not (0 -> false, not 0 -> true)  *)
        Neg*         = 33;                      (*   negate   *)
        Abs*         = 34;                      (*   absolute *)
        Odd*         = 35;                      (*   odd  (first bit = 1)    *)
        Inv*         = 36;                      (*   invert all bits  *)
        Sgn*         = 37;                      (*   sign     *)
        Sqr*         = 38;                      (*   sqr      *)
        Sqrt*        = 39;                      (*   sqrt     *)

        CmpLt*       = 40;                      (*   <   *)
        CmpLe*       = 41;                      (*   <=  *)
        CmpEq*       = 42;                      (*   =   *)
        CmpNeq*      = 43;                      (*   #   *)
        CmpGe*       = 44;                      (*   >=  *)
        CmpGt*       = 45;                      (*   >   *)

        Add*         = 46;                      (*   +   *)
        Sub*         = 47;                      (*   -   *)
        Mul*         = 48;                      (*   *   *)
        Div*         = 49;                      (*   div *)
        Mod*         = 50;                      (*   mod *)
        And*         = 51;                      (*   bit and *)
        Or*          = 52;                      (*   bit or  *)
        Xor*         = 53;                      (*   bit xor *)
        Shl*         = 54;                      (*   shift left *)
        Shr*         = 55;                      (*   shift right *)
        Asr*         = 56;                      (*   arithmetic shift right *)
        Max*         = 57;                      (*   maximum  *)
        Min*         = 58;                      (*   minimum  *)
        Power*       = 59;                      (*   to the power of *)

        CmpLtC*       = 40 + 64;
        CmpLeC*       = 41 + 64;
        CmpEqC*       = 42 + 64;
        CmpNeqC*      = 43 + 64;
        CmpGeC*       = 44 + 64;
        CmpGtC*       = 45 + 64;

        AddC*         = 46 + 64;
        SubC*         = 47 + 64;
        MulC*         = 48 + 64;
        DivC*         = 49 + 64;
        ModC*         = 50 + 64;
        AndC*         = 51 + 64;
        OrC*          = 52 + 64;
        XorC*         = 53 + 64;
        ShlC*         = 54 + 64;
        ShrC*         = 55 + 64;
        AsrC*         = 56 + 64;
        MaxC*         = 57 + 64;
        MinC*         = 58 + 64;
        PowerC*       = 59 + 64;

        CmpLtJ*       = 40 + 128;
        CmpLeJ*       = 41 + 128;
        CmpEqJ*       = 42 + 128;
        CmpNeqJ*      = 43 + 128;
        CmpGeJ*       = 44 + 128;
        CmpGtJ*       = 45 + 128;

        CmpLtCJ*       = 40 + 64 + 128;
        CmpLeCJ*       = 41 + 64 + 128;
        CmpEqCJ*       = 42 + 64 + 128;
        CmpNeqCJ*      = 43 + 64 + 128;
        CmpGeCJ*       = 44 + 64 + 128;
        CmpGtCJ*       = 45 + 64 + 128;


VAR
        MASK32*:   INTEGER;
        FACTOR32*: INTEGER;


PROCEDURE ShowCode* (VAR name: ARRAY OF CHAR; code:INTEGER);
VAR a: ARRAY 32 OF CHAR;
BEGIN
   name := "";
   CASE code OF
    | Null:     name := "NULL"
    | Pad:      name :="PADDING"
    | Unit:     name := "   Unit"
    | Dup:      name := "   Dup"
    | Nip:      name := "   Nip"
    | Over:     name := "  Over"
    | Swap:     name := "  Swap"
    | Zap:      name := "   Zap"

    | Lf:       name := "  -Lf-"
    | Lff:      name := " --LFF--"
    | Spc:      name := "   Spc"
    | Print:    name := "   Print"
    | IPrint:   name := "   IPrint"
    | CPrint:   name := "   CPrint"

    | Exit:     name := "  -EXIT-"
    | Proc:     name := "PROCEDURE"
    | Jmp:      name := "   Jmp"
    | Jeq0:     name := "   Jeq0"
    | Jneq0:    name := "  Jneq0"
    | Call:     name := "   Call"
    | CallInd:  name := "CallInd"
    | Ret:      name := "RETURN"

    | PosLit:   name := "   PosLit"
    | NegLit:   name := "   NegLit"
    | AsGlobal: name := "ASGLOBAL"
    | GlbLoad:  name := "GLBLOAD"
    | StkLoad:  name := "   StkLoad"
    | AsLocal:  name := "AsLocal"
    | MemLoad:  name := "   MemLoad <----"
    | MemStore: name := "MemStore"

    | Syscall:  name := "SYSCALL"
    | Free:     name := " <FREE>"

    | CmpLt:    name := " CmpLt"
    | CmpLe:    name := " CmpLe"
    | CmpEq:    name := " CmpEq"
    | CmpNeq:   name := " CmpNeq"
    | CmpGe:    name := " CmpGe"
    | CmpGt:    name := " CmpGt"
    | Add:      name := "   Add"
    | Sub:      name := "   Sub"
    | Mul:      name := "   Mul"
    | Div:      name := "   Div"
    | Mod:      name := "   Mod"
    | And:      name := "   And"
    | Or:       name := "    Or"
    | Xor:      name := "   Xor"
    | Shl:      name := "   Shl"
    | Shr:      name := "   Shr"
    | Asr:      name := "   Asr"
    | Max:      name := "   Max"
    | Min:      name := "   Min"
    | Power:    name := " Power"
    | Not:      name := "   Not"
    | Neg:      name := "   Neg"
    | Abs:      name := "   Abs"
    | Odd:      name := "   Odd"
    | Inv:      name := "   Inv"
    | Sgn:      name := "   Sgn"
    | Sqr:      name := "   Sqr"
    | Sqrt:     name := "  Sqrt"
   END;
   IF name = "" THEN
      Lib.ShowInt (a, code);
      name := "<";
      Lib.Append (name, a);
      Lib.Append (name, ">");
   END;
END ShowCode;

PROCEDURE Init;
VAR f: INTEGER;
BEGIN
   f := 256;                                    (* in case eoc is in 32-bit-mode   *)
   MASK32   := 0ffffH * f * f + 0ffffH;         (* but here, in ovm, we are 64-bit *)
   FACTOR32 := f * f * f * f;
END Init;

BEGIN

   Init;

END OvmOp.

