## README.md


## Susel  --  A simple expression language.


Susel came into existence when i decided to create  
a small and simple stack based virtual machine  
intended to explore if it could become
a compilation target of some compiler.

So i needed a small and simple language with compiler
to get the vm to work.

This is susel.

Main inspiration comes from BLISS.

BLISS is a huge language. Susel is small.

Susel runs on [ Ovm.ob ]( https://codeberg.org/sts-q/oberon/src/branch/main/oberon-07/modules/Ovm.ob )
and on [ ovmf.fasm ]( https://codeberg.org/sts-q/oberon/src/branch/main/oberon-07/ovmf/ovmf.fasm ).

It's compiler is [ xom.ob ]( https://codeberg.org/sts-q/oberon/src/branch/main/oberon-07/modules/xom.ob ).

Susel happily ignores 60 years of programming language research
and lives where BLISS and BCPL were 1968.  :-)


## Ovm

Ovm is a simple stack based virtual machine.

It is used as a compilation target for the Susel compiler.

Ovm.ob is written in Oberon for eoc, eric's oberon compiler, which transpiles to C.
It should compile and run where ever C compiles.

ovmf.fasm is written in fasm assembly language for intel/amd x86-64 Linux.
All development happens on intel i3-550.

(See links above.)

Ovm is meant to be a compilation target for compilers that is nice and simple and stable.

Ovm bytecode should start up and run fast and could be a starting point for compilation to native code.


### Links

https://www2.cs.arizona.edu/classes/cs520/spring06/bliss.pdf

https://github.com/madisongh/blissc


[ freeputer ](https://github.com/RobertGollagher/Freeputer/tree/master/archive/1.0 )

[ uxn ](https://100r.co/site/uxn.html )


#### IRC
irc.libera.chat #oberon sts-q



