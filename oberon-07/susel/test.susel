-- ============================================================================
module Test  --  :file: test.susel  --  tests


-- ============================================================================
-- :chapter: LIB
fun testFailed (Int a b mess) is                                                -- #TST
        lff;asError;
        lf; print "############  "; print "testFailed";
        lf; print .mess;
        lf; print "expected: "; iout .a;
        lf; print "got:      "; iout .b;
        asStd;
        lf

fun testCase (Int expected got mess) is                                         -- #TST
        | .expected = .got  | unit
        others              | testFailed .expected .got .mess
        end


-- ============================================================================
-- :chapter: SILENT
fun __testStrings is                                                            -- #TST
        Int a b begin
        testCase      0     (strLength "")              "strLength ''";
        testCase      1     (strLength "a")             "strLength 'a'";
        testCase      4     (strLength "otto")          "strLength 'otto'";
        testCase      0     (strCmp "a" "a")            "strCmp: a a ";
        testCase (neg 1)    (strCmp "a" "b")            "strCmp: a b ";
        testCase      1     (strCmp "b" "a")            "strCmp: b a ";
        testCase      0     (strCmp "hello" "hello")    "strCmp: hello hello ";
        testCase      1     (strCmp "helmo" "hello")    "strCmp: helmo hello ";
        testCase (neg 1)    (strCmp "hello" "helmo")    "strCmp: hello helmo ";
        testCase      0     (strCmp "" "")              "strCmp: '' '' ";
        testCase (neg 1)    (strCmp "" "ab")            "strCmp: '' ab ";
        testCase      1     (strCmp "ab" "")            "strCmp: ab '' ";
        unit

fun __testCaldate is                                                            -- #TST
        testCase   True         (isLeapyear 2024)       "leapyear 2024";
        testCase   False        (isLeapyear 2025)       "leapyear 2025";
        testCase   True         (isLeapyear 2000)       "leapyear 2000";
        testCase   False        (isLeapyear 1900)       "leapyear 1900";
        testCase   True         (isValidCaldate 2024 2 29)  "2024 2 29";
        testCase   False        (isValidCaldate 2025 2 29)  "2024 2 29";
        testCase   31           (monthLength 2024 1)        "monthLength 2024 1";
        testCase   30           (monthLength 2024 11)       "monthLength 2024 11";
        unit

fun checkPrime (Int n res) is
        if (isPrime .n) # .res then
           lff; asError; print "########  checkPrime test failed: "; iout .n; bprint .res
        end


--! __isPrime;
fun __isPrime is                                                                -- #TST
        Int n r
        do
--         lf; print "------------ __isPrime";
        checkPrime 0 False;
        checkPrime 1 False;
        checkPrime 2 True;
        checkPrime 3 True;
        checkPrime 5 True;
        checkPrime 7 True;
        checkPrime 13 True;
        checkPrime 13 True;
        checkPrime 16 False;
        checkPrime 17 True;
        checkPrime 21 False;
        checkPrime 23 True;
        checkPrime 29 True;
        checkPrime 30 False;
        checkPrime 31 True;
        checkPrime 32 False;
        checkPrime 37 True;
        checkPrime 71 True;
        checkPrime 111 False;
        checkPrime 112 False;
        checkPrime 119 False;
        checkPrime 997 True;
        checkPrime 1007 False;
        checkPrime 1009 True;
        lff


-- ============================================================================
-- :chapter: VERBOSE
--! __suselBasicTests;
fun __suselBasicTests () is                                                     -- #TST
        section "__suselBasicTests";
        lf; print "+ - * :     "; iout (3 + 4); iout (3 - 4); iout (3 * 4);
        lf; print "power:      "; iout (2 ** 16); iout (3 ** 3); iout (3 ** 4);
        lf; print "and or xor: "; iout (6 /\ 3); iout (6 \/ 3); iout (6 >< 3);
        lf; print "shifting:   "; iout (8 << 2); iout (8 << 1); iout (8 << 0);
        lf; iout (8 >> 2); iout (8 >> 1); iout (8 >> 0);
        lf; iout abs (0 - 12); iout abs 12; iout inv 15; iout neg 12; iout odd 12;
        lf; iout sgn 12; iout sgn (0 - 12); iout sgn 0;
        lf; iout sqr 16; iout sqrt 16;
        lf; iout (1 < 2); iout (2 < 2); iout (3 < 2);
        lf; iout (1 <= 2); iout (2 <= 2); iout (3 <= 2);
        lf; iout (1 >  2); iout (2 >  2); iout (3 > 2);
        lf; iout (1 >= 2); iout (2 >= 2); iout (3 >= 2);
        lf; iout (1 = 2); iout (2 = 2); iout (3 = 2);
        lf; iout (1 # 2); iout (2 # 2); iout (3 # 2);
        lf; iout max 1 2; iout max 2 2; iout max 3 2;
        lf; iout min 1 2; iout min 2 2; iout min 3 2;
        lff


fun __txargs (Int a b c d e) to Int is                                          -- #TST
        Int x y z
        Name aa bb begin
        section "__txargs";
        x <- 10;
        y <- 20;
        z <- 30;
        aa <- 11; (aa + 1) <- 12; (aa + 2) <- 13; (aa + 31) <- 31; (bb + 31) <- 331;
        lf; iprint 12 .a; iprint 12 .b; iprint 12 .c; iprint 12 .d; iprint 12 .e;
        lf; iprint 12 .a; iprint 12 .b; iprint 12 .c; iprint 12 .d; iprint 12 .e;
        lf; iprint 12 .(e - 1); iprint 12 .(e - 2); iprint 12 .(e - 3);
        lf; iprint 12 .(e - 4 - 32); iprint 12 .(e - 31);
        lf; iprint 12 .(e - 33); iprint 12 .(e - 34); iprint 12 .(e - 35);
        lf; iprint 12 .(aa + 0); iprint 12 .(aa + 1); iprint 12 .(aa + 2);
        lff;
        lf; iprint 12 .(a - 0);
        lf; iprint 12 .(a - 1);
        lf; iprint 12 .(a - 2);
        lf; iprint 12 .(a - 3);
        lf; iprint 12 .(a - 4);
        lf; iprint 12 .(a - 5);
        lf; iprint 12 .(a - 6);
        lf; iprint 12 .(a - 7);
        lf; iprint 12 .(a - 8);
        lf; iprint 12 .(a - 8 - 32);
        lff;
        lf


--! __compileData;
fun __compileData () is                                                         -- #TST
        Int str begin
        lff; section "__compileData";
        lf; iout ( [ 12300 101 102 103 ]);
        lf; iout (. [ 12301 201 202 203 ]);
        lf; iout (. [ 12302 301 302 303 ]);
        str <-  [ 10 " --- Hallo, world!!"
                  10" --- This is a string inlined into a fun def!!"
                  10" --- Cool man!!" 0 ];
        print .str;
        lf; print ("Hallo" + 2);
        lf; iout .("Hallo" - 1);
        lf; print ["String length is: "0]; iout .(.str - 1); print ["characters"0];
        lf; iout 60004;
        lf


value messFunAddresses ["function addresses" 0]
fun __funAddresses () is                                                        -- #TST
        Int f begin
        section messFunAddresses;
        lf; iout 60001;
        lf; iout `asSection; iout `asText; iout `section; iout `__funAddresses;
        lf; iout  6; iout (`fac $  6);
        lf; iout  7; iout (`fac $  7);
        lf; iout 10; iout (`fib $ 10);
        lf; iout  5; f <- `fac; iout (.f $ 5);
        lf; iout 60002;
        lf

value afun_pos [ 100 202 331 ]
fun afun (Int a) to Int is  .a + .a
fun __callind () is                                                             -- #TST
        Int f begin
        f <- (afun_pos + 2);
        cprint '<'; iout ((1 +.f) $ 12); cprint '>';
        lf


-- value intString [ "<................................>" 0 ]
--! __intShow;
fun __intShow () is                                                             -- #TST
        Int x   Name intString begin
        section "__intShow";
        lf; x <- 1234;          print (intShow intString  8 .x); cprint '>';
        lf; x <- (0 - 1234);    print (intShow intString  8 .x); cprint '>';
        lf; x <- 12;            print (intShow intString  8 .x); cprint '>';
        lf; x <- 1;             print (intShow intString  8 .x); cprint '>';
        lf; x <- 0;             print (intShow intString  8 .x); cprint '>';
        lf; x <- (0 - 1);       print (intShow intString  8 .x); cprint '>';
        lf; x <- 9976541000;    print (intShow intString  8 .x); cprint '>';
        lf; x <- 8;             print (intShow intString  8 .x); cprint '>';
        lf; x <- (0 - 8);       print (intShow intString  8 .x); cprint '>';
        lff;
        lf; x <- (7);           print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (10);          print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (15);          print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (16);          print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (1600);        print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (160000);      print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (16777216);    print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (1677721600);  print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (0x2_0000);    print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (0x20_0000);   print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (0x20_FFaa);   print (intShowHex intString  19 .x); cprint '>';
        lf; x <- (0xffFF_FFff); print (intShowHex intString  19 .x); cprint '>';
        lf

fun __fac () is                                                                 -- #TST
        section "__fac";
        lff;fiout facPos 1;
        lf; fiout facPos 4;
        lf; fiout facPos 5;
        lf; fiout facPos 6;
        lf; fiout facPos 7;
        lf; fiout facPos 10;
        lf


fun __fib () is                                                                 -- #TST
        Int f
        do
        section "__fib";
        f <- `fib;
        lf; fiout .f 10;
        lf; fiout .f 20;
--         lf; fiout .f 36;
--         lf; fiout .f 42;
        lf

--! __rnd;
fun __rnd is                                                                    -- #TST
        Int sum n x i begin
        section "__rnd";
        lf;
        n <- 10;
        sum <- 0;
        i <- 0;
        loop exit .i = .n do
           x <- rndRoll .n;
           iout .x;
           add sum .x;
           inc i
        end;
        lf; print "rndRoll: "; iout .n; iout (.sum div .n);
        i <- 0;
        sum <- 0;
        loop exit .i = .n do
           add sum rndBool;
           inc i
        end;
        lf; print "rndBool: "; iout .n; iout .sum;
        lf


--! __caldate;
fun __caldate is                                                                -- #TST
        Int aa bb cc dd ee gg Caldate cd   Name a   Int y m d   Daynum dn dn2  begin
        section "__caldate";
        dn <- 1;
        loop exit .dn > LastValidDaynum do
           cldCaldate cd .dn;
           dn2 <- cldDaynum .cd .(cd+1) .(cd+2);
           assert (.dn = .dn2) "caldate messed up";
           if .dn mod 4013 = 0 then lf; iout .dn; cldPrint .dn; memDump cd 3; iout .dn2 end;
           inc dn
        end;
        lff;
        lf; y <- 1900; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
        lf; y <- 1900; m <-  1; d <-  2;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
        lf; y <- 1900; m <-  1; d <-  3;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1900; m <-  1; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1900; m <- 12; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1901; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1901; m <- 12; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1902; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1903; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1903; m <- 12; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1904; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 1904; m <- 12; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 2001; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 2001; m <- 12; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 2002; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 2099; m <- 12; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 2100; m <-  1; d <-  1;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
--         lf; y <- 2100; m <- 12; d <- 31;   dn <- cldDaynum .y .m .d; memDump y 4; cldPrint .dn;
        lff;
        lf; dn <- cldDaynum 1901  1  1; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 1901/1/1";
        lf; dn <- cldDaynum 1901  1  1; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 1901/1/1";
        lf; dn <- cldDaynum 2023 12 30; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 23/12/30";
--         lf; dn <- cldDaynum 2023 12 31; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 23/12/31";
--         lf; dn <- cldDaynum 2024  1  1; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 24/1/1";
--         lf; dn <- cldDaynum 2024 11 30; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 24/11/30";
--         lf; dn <- cldDaynum 2024 12 01; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 24/12/01";
--         lf; dn <- cldDaynum 2024 12 30; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 24/12/30";
--         lf; dn <- cldDaynum 2024 12 31; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 24/12/31 ";
--         lf; dn <- cldDaynum 2025  1  1; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 25/1/1";
--         lf; dn <- cldDaynum 2025 12 31; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 25/12/31";
--         lf; dn <- cldDaynum 2026  1  1; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 26/1/1";
--         lf; dn <- cldDaynum 2025  1  2; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 25/1/2";
--         lf; dn <- cldDaynum 2099 12 31; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 2099/12/31";
--         lf; dn <- cldDaynum 2100  1  1; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 2100/1/1";
--         lf; dn <- cldDaynum 2100 12 31; cldCaldate cd .dn; iout .dn; cldPrint .dn; print " 2100/12/31";
        lff;
        lf; now a;       print "now:       "; print a;
        lf; today a;     print "today:     "; print a;
        lf; today_now a; print "today now: "; print a;
        lf; bprint isLeapyear 1900;
        lf; bprint isLeapyear 2000;
        lf; bprint isLeapyear 2100;
        lf; iout (cldDaynum 2025 12 24 - cldToday); print "days until next chrismas!";
        unit


-- ============================================================================
-- :chapter: MAIN
fun __testSilent is                                                             -- #TST
        chapter "__testSilent";
        __testStrings;
        __testCaldate;
        __isPrime;
        unit


-- ============================================================================
end Test.  -- :file: END sts-q 2024-Dec

